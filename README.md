# Greeter

This is a small script that writes a greeting whenever you open a terminal.

![screenshot](Screenshot.png)

## Installation

1. Install Python 3
2. Add the following line to the end of your ~/.bashrc file:
`python3 /path/to/script/greeter.py`
3. Done!
