# Greeter
# by
# Caton101

import random
import os
from math import floor

# get terminal width
screen = os.get_terminal_size()
window_width = screen.columns

# needed for box generation
bar = ""
padding_left = ""
padding_right = ""

# list of greetings
lines = [
    "Welcome!",
    "This is a really really really really long line and it takes up a lot of space.",
    "Did you know 9/10 nerds prefer Arch Linux?",
    "At your service, master.",
    "Ready to hack the world?",
    "All systems go.",
    "/r/linuxmasterrace",
    "It is going to be a great day.",
    "The system is in your hands.",
    "In Linus we trust.",
    "I'd just like to interject for a moment.",
    "aplay /bin/bash",
    "In Torvalds we trust."
    ]

# select a line to write
to_write = lines[random.randint(0,len(lines)-1)]

# ensure line will fit terminal
if (len(to_write) + 4) > window_width:
    # shorten text
    new_length = window_width - 7
    # check for space as last character
    while to_write[new_length - 1] == " ":
        # remove space from line
        new_length -= 1
        # check if entire line was spaces
        if new_length == 0:
            break
    # add ...
    to_write = to_write[:new_length] + "..."

# get left gap size
gap_left = floor((window_width - len(to_write) - 4)/2)
# set right gap size
gap_right = gap_left
# if center line is too small, add another space to the right side
if (len(to_write) + gap_left + gap_right + 4) < window_width:
    gap_right += 1

# generate left side padding
for i in range(gap_left):
    padding_left += " "

# generate right side padding
if gap_left == gap_right:
    padding_right = padding_left
else:
    for i in range(gap_right):
        padding_right += " "

# generate horizontal lines for box
for lap in range(window_width):
    bar += "~"

# write it out
print(bar)
print("~ %s%s%s ~" % (padding_left,to_write,padding_right))
print(bar)
exit()
